<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/estates', function (Request $request) {
    $results = DB::select('select * from estates');
    return response()->json($results, 200);
});

Route::get('/estates/{idestate}', function ($idestate) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (estateNotExists($idestate)) {
        abort(404);
    }

    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from estates where idestate=:idestate', [
        'idestate' => $idestate,
    ]);

    // Así se devuelve un JSON.
    // El primer parámetro es el dato que se manda como JSON.
    // El segundo parámetro es el status code
    return response()->json($results[0], 200);
});
Route::post('/estates', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into estates (idestate, urlphoto, operation, type, province, location, zone, rooms, bathrooms, elevator, sqrtfeet, externalfeat, internalfeat, price, datecreate)
        values (:idestate, :urlphoto, :operation, :type, :province, :location, :zone, :rooms, :bathrooms, :elevator, :sqrtfeet, :externalfeat, :internalfeat, :price, :datecreate)
    ",
        $data
    );

    $results = DB::select('select * from estates where idestate = :idestate', [
        'idestate' => $data['idestate'],
    ]);
    return response()->json($results[0], 200);
});

Route::put('/estates/{idestate}', function ($idestate) {
    if (estateNotExists($idestate)) {
        abort(404);
    }
    $data = request()->all();

    DB::delete(
        "
        delete from estates where idestate = :idestate",
        ['idestate' => $idestate]
    );

    DB::insert(
        "
        insert into estates (idestate, urlphoto, operation, type, province, location, zone, rooms, bathrooms, elevator, sqrtfeet, externalfeat, internalfeat, price, datecreate)
        values (:idestate, :urlphoto, :operation, :type, :province, :location, :zone, :rooms, :bathrooms, :elevator, :sqrtfeet, :externalfeat, :internalfeat, :price, :datecreate)
    "    
    ,
        $data
    );

    $results = DB::select('select * from estates where idestate = :idestate', ['idestate' => $idestate]);
    return response()->json($results[0], 200);
});

Route::patch('/estates/{idestate}', function ($idestate) {
    if (estateNotExists($idestate)) {
        abort(404);
    }
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['idestate'] = $idestate;

    DB::update(
        'update estates SET ' . join(', ', $update_statements) . ' where idestate = :idestate',
        $data
    );

    $results = DB::select('select * from estates where idestate = :idestate', ['idestate' => $idestate]);
    return response()->json($results[0], 200);
});

Route::delete('/estatesDelete/{idestate}', function ($idestate) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (estateNotExists($idestate)) {
        abort(404);
    }

    DB::delete('delete from estates where idestate = :idestate', ['idestate' => $idestate]);

    return response()->json('delete completed', 200);
});

Route::get('/search/estates/{operation}', function ($operation) {
    
    if (operationNoExists($operation)) {
        abort(404);
    }
  
    $results = DB::select('select * from estates where operation=:operation', [
        'operation' => $operation,
    ]);
    return response()->json($results, 200);
});

// Laravel tiene un fallo y carga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('estateNotExists')) {
    function estateNotExists($idestate)
    {
        $results = DB::select('select * from estates where idestate=:idestate', [
            'idestate' => $idestate,
        ]);

        return count($results) == 0;
    }
}

if (!function_exists('operationNoExists')) {
    function operationNoExists($operation)
    {
        $results = DB::select('select * from estates where operation=:operation', [
            'operation' => $operation,
        ]);
        return count($results) == 0;
    }
}
