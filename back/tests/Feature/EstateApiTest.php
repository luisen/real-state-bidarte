<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class EstateApiTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE estates (
                idestate	TEXT NOT NULL,
                urlphoto    TEXT,                
                operation	TEXT NOT NULL,
                type	TEXT NOT NULL,
                province    TEXT NOT NULL,
                location	TEXT NOT NULL,   
                zone	TEXT NOT NULL,                             
                rooms   INTEGER,
                bathrooms   INTEGER,
                elevator    TEXT,
                sqrtfeet    INTEGER NOT NULL,
                externalfeat    TEXT NOT NULL,
                internalfeat    TEXT NOT NULL,
                price   INTEGER NOT NULL,
                datecreate  TEXT NOT NULL,
                PRIMARY KEY(idestate)
            );
            INSERT INTO estates VALUES ('1','url1', 'alquiler', 'piso', 'Bizkaia','Bilbao', 'Miribilla', '3', '1', 'si', '65', 'ext1', 'int1', '850', '2020-04-1');
            INSERT INTO estates VALUES ('2','url2', 'venta', 'piso', 'Bizkaia','Bilbao', 'Deusto', '4', '2', 'si', '75', 'ext2', 'int2', '250000', '2020-04-2');
        ");
        // $this->withoutExceptionHandling();
    }

    public function testGetEstates()
    {
        $this->json('GET', 'api/estates')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'idestate' => '1',
                    'urlphoto' => 'url1',
                    'operation' => 'alquiler',
                    'type' => 'piso',
                    'province' => 'Bizkaia',
                    'location' => 'Bilbao',
                    'zone' => 'Miribilla',
                    'rooms' => '3',
                    'bathrooms' => '1',
                    'elevator' => 'si',
                    'sqrtfeet' => '65',
                    'externalfeat' => 'ext1',
                    'internalfeat' => 'int1',
                    'price' => '850',
                    'datecreate' => '2020-04-1',
                ],
                [
                    'idestate' => '2',
                    'urlphoto' => 'url2',
                    'operation' => 'venta',
                    'type' => 'piso',
                    'province' => 'Bizkaia',
                    'location' => 'Bilbao',
                    'zone' => 'Deusto',
                    'rooms' => '4',
                    'bathrooms' => '2',
                    'elevator' => 'si',
                    'sqrtfeet' => '75',
                    'externalfeat' => 'ext2',
                    'internalfeat' => 'int2',
                    'price' => '250000',
                    'datecreate' => '2020-04-2',
                ],
            ]);
    }

    public function testGetEstate()
    {
        $this->json('GET', 'api/estates/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'idestate' => '1',
                    'urlphoto' => 'url1',
                    'operation' => 'alquiler',
                    'type' => 'piso',
                    'province' => 'Bizkaia',
                    'location' => 'Bilbao',
                    'zone' => 'Miribilla',
                    'rooms' => '3',
                    'bathrooms' => '1',
                    'elevator' => 'si',
                    'sqrtfeet' => '65',
                    'externalfeat' => 'ext1',
                    'internalfeat' => 'int1',
                    'price' => '850',
                    'datecreate' => '2020-04-1',
                ]
            );
    }

    public function testPostEstate()
    {
        $this->json('POST', 'api/estates', [
            'idestate' => '99',
            'urlphoto' => 'url3',
            'operation' => 'alquiler',
            'type' => 'lonja',
            'province' => 'Gipuzkoa',
            'location' => 'Donosti',
            'zone' => 'Gros',
            'rooms' => '1',
            'bathrooms' => '1',
            'elevator' => 'no',
            'sqrtfeet' => '45',
            'externalfeat' => 'ext3',
            'internalfeat' => 'int3',
            'price' => '1125',
            'datecreate' => '2020-04-3',
        ])->assertStatus(200);

        $this->json('GET', 'api/estates/99')
            ->assertStatus(200)
            ->assertJson([
                'idestate' => '99',
                'urlphoto' => 'url3',
                'operation' => 'alquiler',
                'type' => 'lonja',
                'province' => 'Gipuzkoa',
                'location' => 'Donosti',
                'zone' => 'Gros',
                'rooms' => '1',
                'bathrooms' => '1',
                'elevator' => 'no',
                'sqrtfeet' => '45',
                'externalfeat' => 'ext3',
                'internalfeat' => 'int3',
                'price' => '1125',
                'datecreate' => '2020-04-3',
            ]);
    }

    public function testPut()
    {
        $data = [
            'idestate' => '1',
            'urlphoto' => 'url1',
            'operation' => 'alquiler',
            'type' => 'piso',
            'province' => 'Bizkaia',
            'location' => 'Bilbao',
            'zone' => 'Sarriko',
            'rooms' => '3',
            'bathrooms' => '1',
            'elevator' => 'si',
            'sqrtfeet' => '65',
            'externalfeat' => 'ext1',
            'internalfeat' => 'int1',
            'price' => '850',
            'datecreate' => '2020-04-1',
        ];

        $expected = [
            'idestate' => '1',
            'urlphoto' => 'url1',
            'operation' => 'alquiler',
            'type' => 'piso',
            'province' => 'Bizkaia',
            'location' => 'Bilbao',
            'zone' => 'Sarriko',
            'rooms' => '3',
            'bathrooms' => '1',
            'elevator' => 'si',
            'sqrtfeet' => '65',
            'externalfeat' => 'ext1',
            'internalfeat' => 'int1',
            'price' => '850',
            'datecreate' => '2020-04-1',
        ];

        $this->json('PUT', 'api/estates/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

        $this->json('GET', 'api/estates/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }

/*     public function testPatch()
    {
        $this->json('PATCH', 'api/cars/1', [
            'registration' => 'AAA002',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-01',
                'color' => 'red',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);

        $this->json('GET', 'api/cars/1')
            ->assertStatus(200)
            ->assertJson([
                'registration' => 'AAA002',
            ]);

        $this->json('PATCH', 'api/cars/1', [
            'dateRegistered' => '2019-01-02',
            'color' => 'black',
            'make' => 'KIA',
            'model' => 'Carens',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);

        $this->json('GET', 'api/cars/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]);
    } */

    public function testDeleteEstate()
    {
        $this->json('GET', 'api/estatesDelete/1')->assertStatus(200);

        $this->json('DELETE', 'api/estatesDelete/1')->assertStatus(200);

        $this->json('GET', 'api/estatesDelete/1')->assertStatus(404);
    }

/*     public function testGetCarNotExist()
    {
        // $this->json('GET', 'api/cars/22')->assertStatus(404);

        $this->json('DELETE', 'api/cars/22')->assertStatus(404);

        $this->json('PUT', 'api/cars/22', 
            [
                'id' => '1',
                'registration' => 'AAA002',
                'dateRegistered' => '2019-01-02',
                'color' => 'black',
                'make' => 'KIA',
                'model' => 'Carens',
            ]
        )
            ->assertStatus(404);

        $this->json('PATCH', 'api/cars/22', [
            'registration' => 'AAA002',
        ])
            ->assertStatus(404);
    } */

}