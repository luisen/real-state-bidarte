import { shallowMount } from '@vue/test-utils'
import EstateList from '@/components/EstateList/EstateListPage.vue'
import EstateCard from '@/components/EstateList/EstateCard.vue'
import api from '@/api.js'

test('estado initial', () => {
  api.getEstates = jest.fn()

  const wrapper = shallowMount(EstateList, {
    data() {
      return {
        estates: []
      }
    }
  })
  const estateCard = wrapper.findAll(EstateCard).wrappers

  expect(estateCard.length).toBe(0)
})

test('En Estatelist se pinta todo lo que contiene estates', () => {
  api.getEstates = jest.fn()
  const wrapper = shallowMount(EstateList, {
    data() {
      return {
        estates: [
          {
            idestate: '0000001',
            price: 850,
            rooms: 3,
            bathrooms: 1,
            sqrtfeet: 75
          },
          {
            idestate: '0000002',
            price: 250000,
            rooms: 4,
            bathrooms: 2,
            sqrtfeet: 80
          }
        ]
      }
    }
  })
  const estateCard = wrapper.findAll(EstateCard).wrappers

  expect(estateCard.length).toEqual(2)
})

const finishAsyncTasks = () => new Promise(setImmediate)

test('al montar el componente se ejecuta getEstates() si params es undefined ', async () => {
  api.getEstates = jest.fn()
  const mockEstates = [
    {
      idestate: '0000001',
      price: 850,
      rooms: 3,
      bathrooms: 1,
      sqrtfeet: 75
    },
    {
      idestate: '0000002',
      price: 250000,
      rooms: 4,
      bathrooms: 2,
      sqrtfeet: 80
    }
  ]
  api.getEstates.mockReturnValue(mockEstates)
  const wrapper = shallowMount(EstateList, {
    data() {
      return {
        estates: []
      }
    }
  })

  await finishAsyncTasks()
  expect(api.getEstates).toHaveBeenCalled()
  expect(wrapper.vm.estates).toEqual(mockEstates)
})
