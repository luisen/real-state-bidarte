import { shallowMount } from '@vue/test-utils'
import EstateCard from '@/components/EstateList/EstateCard.vue'

test('pinta en la pantalla', () => {
  const wrapper = shallowMount(EstateCard, {
    propsData: {
      estate: {
        idestate: '0000001',
        // operation: 'alquiler',
        // type: 'piso',
        // location: 'Bilbao',
        // zone: 'Santutxu',
        price: 850,
        rooms: 3,
        bathrooms: 1,
        sqrtfeet: 75
      }
    }
  })
  // expect(wrapper.text()).toContain('alquiler')
  // expect(wrapper.text()).toContain('piso')
  // expect(wrapper.text()).toContain('Bilbao')
  // expect(wrapper.text()).toContain('Santutxu')
  expect(wrapper.text()).toContain('850')
  expect(wrapper.text()).toContain('3')
  expect(wrapper.text()).toContain('1')
  expect(wrapper.text()).toContain('75')
})

test('Se envía el evento estateDetails', async () => {
  const estate = {
    idestate: '0000001',
    price: 850,
    rooms: 3,
    bathrooms: 1,
    sqrtfeet: 75
  }
  const wrapper = shallowMount(EstateCard, {
    propsData: { estate: estate }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const buttonDetails = wrapper.find('.btn-details')
  buttonDetails.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['estateDetails'].length).toBe(1)
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'estateDetails',
      args: ['0000001']
    }
  ])
})

test(' que se envía el evento deleteEstate', async () => {
  const estate = {
    idestate: '0000001',
    price: 850,
    rooms: 3,
    bathrooms: 1,
    sqrtfeet: 75
  }
  const wrapper = shallowMount(EstateCard, {
    propsData: { estate: estate }
  })

  expect(wrapper.emitted().click).toBe(undefined)

  const buttonDelete = wrapper.find('.btn-delete')
  buttonDelete.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['deleteEstate'].length).toBe(1)
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: 'deleteEstate',
      args: ['0000001']
    }
  ])
})

// test('Crear un Computed para fullEstate', async () => {
//   const wrapper = shallowMount(EstateCard, {
//     propsData: {
//       details: {
//         id: '01',
//         operation: 'alquiler',
//         type: 'piso',
//         location: 'Bilbao',
//         zone: 'Santutxu'
//       }
//     }
//   })
//   await wrapper.vm.$nextTick()
//   expect(wrapper.vm.fullEstate).toContain('alquiler, piso, Bilbao, Santutxu')
// })
