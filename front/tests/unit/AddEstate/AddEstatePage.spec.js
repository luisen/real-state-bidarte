import { shallowMount } from '@vue/test-utils'
import AddEstatePage from '@/components/AddEstate/AddEstatePage.vue'

test('emite evento upload newEstate  ', async () => {
  const wrapper = shallowMount(AddEstatePage, {
    data() {
      return {
        estate: {
          hasImage: false,
          image: null,
          operation: null,
          type: null,
          province: null,
          location: null,
          zone: null,
          rooms: null,
          bathrooms: null,
          elevator: null,
          sqrtfeet: null,
          externalfeat: null,
          internalfeat: null,
          price: null,
          datecreate: null
        }
      }
    }
  })

  expect(wrapper.emitted().search).toBe(undefined)

  const inputProvince = wrapper.find('#province')
  const inputLocality = wrapper.find('#location')
  const inputZone = wrapper.find('#zone')
  const inputRooms = wrapper.find('#rooms')
  const inputBathrooms = wrapper.find('#bathrooms')
  const inputSqrtfeet = wrapper.find('#sqrtfeet')
  const textAreaInternalFeet = wrapper.find('#internalfeat')
  const textAreaExternalFeet = wrapper.find('#externalfeat')
  const inputInputPrice = wrapper.find('#price')
  const btnAddEstate = wrapper.find('.btaddEstate')

  inputProvince.setValue('Bizkaia')
  inputLocality.setValue('Bilbao')
  inputZone.setValue('Santutxu')
  inputRooms.setValue('4')
  inputBathrooms.setValue('1')
  inputSqrtfeet.setValue('75')
  textAreaInternalFeet.setValue(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
  )
  textAreaExternalFeet.setValue(
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
  )
  inputInputPrice.setValue('850')

  btnAddEstate.trigger('click')
  await wrapper.vm.$nextTick()

  expect(wrapper.emitted()['uploadNewEstate'].length).toBe(1)
  expect(wrapper.emitted()['uploadNewEstate']).toEqual([
    [
      {
        hasImage: false,
        image: null,
        operation: 'alquiler',
        type: 'piso',
        province: 'Bizkaia',
        location: 'Bilbao',
        zone: 'Deusto',
        rooms: '4',
        bathrooms: '1',
        elevator: 'si',
        sqrtfeet: '75',
        externalfeat:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        internalfeat:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        price: '850',
        datecreate: null
      }
    ]
  ])
})
