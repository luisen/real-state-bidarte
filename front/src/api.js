import { v4 as uuidv4 } from 'uuid'
const root = '/api'

export default {
  async getEstates() {
    const result = await fetch(`${root}/estates`)
    return await result.json()
  },

  async getEstatesById(idestate) {
    const result = await fetch(`${root}/estates/` + idestate)
    return await result.json()
  },

  async uploadNewEstate(estate) {
    const result = await fetch(`${root}/estates`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idestate: uuidv4(),
        urlphoto: estate.image.dataUrl,
        operation: estate.operation,
        type: estate.type,
        province: estate.province,
        location: estate.location,
        zone: estate.zone,
        rooms: estate.rooms,
        bathrooms: estate.bathrooms,
        elevator: estate.elevator,
        sqrtfeet: estate.sqrtfeet,
        externalfeat: estate.externalfeat,
        internalfeat: estate.internalfeat,
        price: estate.price,
        datecreate: estate.datecreate
      })
    })
    estate.operation = ''
    estate.type = ''
    estate.province = ''
    estate.location = ''
    estate.zone = ''
    estate.rooms = ''
    estate.bathrooms = ''
    estate.elevator = ''
    estate.sqrtfeet = ''
    estate.externalfeat = ''
    estate.internalfeat = ''
    estate.price = ''
    return await result.json()
  },

  async updateEstate(idestate, localEstate) {
    const result = await fetch(`${root}/estates/` + idestate, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        idestate: localEstate.idestate,
        urlphoto: localEstate.image,
        operation: localEstate.operation,
        type: localEstate.type,
        province: localEstate.province,
        location: localEstate.location,
        zone: localEstate.zone,
        rooms: localEstate.rooms,
        bathrooms: localEstate.bathrooms,
        elevator: localEstate.elevator,
        sqrtfeet: localEstate.sqrtfeet,
        externalfeat: localEstate.externalfeat,
        internalfeat: localEstate.internalfeat,
        price: localEstate.price,
        datecreate: new Date().toLocaleString()
      })
    })
    return await result.json()
  },

  async deleteEstate(idestate) {
    const result = await fetch(`${root}/estatesDelete` + '/' + idestate, {
      method: 'delete'
    })
    return await result.json()
  },

  async search(operation) {
    const result = await fetch(`${root}/search/estates/` + operation)
    return await result.json()
  }
}
