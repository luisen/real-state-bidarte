import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/components/Home/HomePage.vue')
  },
  {
    path: '/contact',
    component: () => import('@/components/Contact/ContactPage.vue')
  },
  {
    path: '/estates',
    component: () => import('@/components/EstateList/EstateListPage.vue')
  },
  {
    path: '/add-estate',
    component: () => import('@/components/AddEstate/AddEstatePage.vue')
  },

  {
    path: '/aviso-legal',
    component: () => import('@/components/page/AvisoLegal.vue')
  },

  {
    path: '/estate/:id',
    name: 'Estate',
    component: () => import('@/components/Estate/EstatePage.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
